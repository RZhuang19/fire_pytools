import numpy as np
import pandas as pd


def weighted_average(data, variable, weight):
    """Calculate value-weighted average."""
    return (data[variable] * data[weight]).sum() / data[weight].sum()


def wavg(x, col, weight=None):
    """weighted average for more than a column at once."""
    return pd.Series(np.average(x[col], axis=0, weights=x[weight]), index=col)
