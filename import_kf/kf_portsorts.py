"""
Get sorted portfolio data from Ken French database.

Todo:
    * Extend to include tables past "Average Market Cap" (not available for all portfolios).
    * Extend to "without dividend" files.
"""
import numpy as np

import warnings

import pandas_datareader.data as web


VARIABLE_KEY_FOLIOS = {'daily': {'ret': 0,
                                 'ret_ew': 1},
                       'weekly': {'ret': 0,
                                  'ret_ew': 1,
                                  'n_firms': 2,
                                  'avg_size': 3,
                                  'avg_char': 6},
                       'monthly': {'ret': 0,
                                   'ret_ew': 1,
                                   'n_firms': 4,
                                   'avg_size': 5},
                       'annual': {'ret': 2,
                                  'ret_ew': 3}
                       }


def list_available_files():
    print('''Available sort portfolios:

             Portfolios_Formed_on_ME
             Portfolios_Formed_on_BE-ME
             Portfolios_Formed_on_OP *
             Portfolios_Formed_on_INV *

             6_Portfolios_2x3 (freq='weekly' available)
             ! 25_Portfolios_5x5
             ! 100_Portfolios_10x10
             6_Portfolios_ME_OP_2x3
             ! 25_Portfolios_ME_OP_5x5
             ! 100_Portfolios_ME_OP_10x10
             6_Portfolios_ME_INV_2x3
             ! 25_Portfolios_ME_INV_5x5
             ! 100_Portfolios_ME_INV_10x10
             ! 25_Portfolios_BEME_OP_5x5
             ! 25_Portfolios_BEME_INV_5x5
             ! 25_Portfolios_OP_INV_5x5

             ! 32_Portfolios_ME_BEME_OP_2x4x4 *
             ! 32_Portfolios_ME_BEME_INV_2x4x4 *
             ! 32_Portfolios_ME_OP_INV_2x4x4 *

             ! Portfolios_Formed_on_E-P *
             ! Portfolios_Formed_on_CF-P *
             ! Portfolios_Formed_on_D-P *

             6_Portfolios_ME_Prior_12_2
             ! 25_Portfolios_ME_Prior_12_2
             10_Portfolios_Prior_12_2
             ! 6_Portfolios_ME_Prior_1_0
             ! 25_Portfolios_ME_Prior_1_0
             ! 10_Portfolios_Prior_1_0
             ! 6_Portfolios_ME_Prior_60_13
             ! 25_Portfolios_ME_Prior_60_13
             ! 10_Portfolios_Prior_60_13

             ! Portfolios_Formed_on_AC *
             ! 25_Portfolios_ME_AC_5x5 *
             ! Portfolios_Formed_on_BETA *
             ! 25_Portfolios_ME_BETA_5x5 *
             ! Portfolios_Formed_on_NI *
             ! 25_Portfolios_ME_NI_5x5 *
             ! Portfolios_Formed_on_VAR *
             ! 25_Portfolios_ME_VAR_5x5 *
             ! Portfolios_Formed_on_RESVAR *
             ! 25_Portfolios_ME_RESVAR_5x5 *

             ! Global_6_Portfolios_ME_BE-ME
             ! Global_ex_US_6_Portfolios_ME_BE-ME
             ! Europe_6_Portfolios_ME_BE-ME
             ! Japan_6_Portfolios_ME_BE-ME
             ! Asia_Pacific_ex_Japan_6_Portfolios_ME_BE-ME
             ! North_America_6_Portfolios_ME_BE-ME

             ! Global_25_Portfolios_ME_BE-ME
             ! Global_ex_US_25_Portfolios_ME_BE-ME
             ! Europe_25_Portfolios_ME_BE-ME
             ! Japan_25_Portfolios_ME_BE-ME
             ! Asia_Pacific_ex_Japan_25_Portfolios_ME_BE-ME
             ! North_America_25_Portfolios_ME_BE-ME

             ! Global_6_Portfolios_ME_OP
             ! Global_ex_US_6_Portfolios_ME_OP
             ! Europe_6_Portfolios_ME_OP
             ! Japan_6_Portfolios_ME_OP
             ! Asia_Pacific_ex_Japan_6_Portfolios_ME_OP
             ! North_America_6_Portfolios_ME_OP

             ! Global_25_Portfolios_ME_OP
             ! Global_ex_US_25_Portfolios_ME_OP
             ! Europe_25_Portfolios_ME_OP
             ! Japan_25_Portfolios_ME_OP
             ! Asia_Pacific_ex_Japan_25_Portfolios_ME_OP
             ! North_America_25_Portfolios_ME_OP

             ! Global_6_Portfolios_ME_INV
             ! Global_ex_US_6_Portfolios_ME_INV
             ! Europe_6_Portfolios_ME_INV
             ! Japan_6_Portfolios_ME_INV
             ! Asia_Pacific_ex_Japan_6_Portfolios_ME_INV
             ! North_America_6_Portfolios_ME_INV

             ! Global_25_Portfolios_ME_INV
             ! Global_ex_US_25_Portfolios_ME_INV
             ! Europe_25_Portfolios_ME_INV
             ! Japan_25_Portfolios_ME_INV
             ! Asia_Pacific_ex_Japan_25_Portfolios_ME_INV
             ! North_America_25_Portfolios_ME_INV

             ! Global_6_Portfolios_ME_Prior_12_2
             ! Global_ex_US_6_Portfolios_ME_Prior_12_2
             ! Europe_6_Portfolios_ME_Prior_12_2
             ! Japan_6_Portfolios_ME_Prior_12_2
             ! Asia_Pacific_ex_Japan_6_Portfolios_ME_Prior_12_2
             ! North_America_6_Portfolios_ME_Prior_12_2

             ! Global_25_Portfolios_ME_Prior_12_2
             ! Global_ex_US_25_Portfolios_ME_Prior_12_2
             ! Europe_25_Portfolios_ME_Prior_12_2
             ! Japan_25_Portfolios_ME_Prior_12_2
             ! Asia_Pacific_ex_Japan_25_Portfolios_ME_Prior_12_2
             ! North_America_25_Portfolios_ME_Prior_12_2

             * freq='daily' not available.
             ! Functionality not tested. Make sure variables contain correct data.
          ''')

    pass


def download(ff_folio_file, freq):
    """
    Query Fama-French portfolios.

    Parameters
    ----------
    ff_folio_file: string
        Name of portfolio file. See list_available_files() for a list of available portfolios.
    freq: string
        Data frequency. Must be in ['daily', 'weekly', 'monthly', 'annual'].
        freq='weekly' is only available for ff_folio_file='6_Portfolios_2x3'.

    Examples
    --------
    df = download('Portfolios_Formed_on_ME', 'monthly')
    """
    assert freq in ['daily', 'weekly', 'monthly', 'annual'], "freq must be 'daily', 'weekly', 'monthly' or 'annual'"
    if freq == 'weekly':
        assert ff_folio_file == '6_Portfolios_2x3', \
            "freq='weekly' is only available for ff_folio_file='6_Portfolios_2x3'"

    if freq == 'daily':
        if ff_folio_file in ['Global_6_Portfolios_ME_Prior_12_2',  # These have different name format for daily file.
                             'Global_ex_US_6_Portfolios_ME_Prior_12_2',
                             'Europe_6_Portfolios_ME_Prior_12_2',
                             'Japan_6_Portfolios_ME_Prior_12_2',
                             'Asia_Pacific_ex_Japan_6_Portfolios_ME_Prior_12_2',
                             'North_America_6_Portfolios_ME_Prior_12_2',
                             'Global_25_Portfolios_ME_Prior_12_2',
                             'Global_ex_US_25_Portfolios_ME_Prior_12_2',
                             'Europe_25_Portfolios_ME_Prior_12_2',
                             'Japan_25_Portfolios_ME_Prior_12_2',
                             'Asia_Pacific_ex_Japan_25_Portfolios_ME_Prior_12_2',
                             'North_America_25_Portfolios_ME_Prior_12_2']:
            print("Daily version of {} uses stock's cumulative return for day t–250 to t–20.".format(ff_folio_file))
            ff_folio_file = ff_folio_file[:-4] + '250_20'

        ff_folio_file = ff_folio_file + '_daily'
    elif freq == 'weekly':
        ff_folio_file = ff_folio_file + '_weekly'

    df = web.DataReader(ff_folio_file, 'famafrench')

    for key, elem in VARIABLE_KEY_FOLIOS[freq].items():
        tempdf = df[elem].reset_index()

        # Remove trailing whitespaces from column names
        tempdf.columns = tempdf.columns.str.strip()

        tempdf = tempdf.melt(id_vars='Date', var_name='portfolio',
                             value_name=key)
        if 'outdf' not in locals():
            outdf = tempdf
        else:
            outdf = outdf.merge(tempdf, on=['Date', 'portfolio'])

    outdf.rename(columns={'Date': 'date'}, inplace=True)
    try:
        outdf['date'] = outdf['date'].dt.to_timestamp(how='end')
    except AttributeError:
        pass

    outdf.replace(to_replace=[-99.99, -999], value=np.nan, inplace=True)

    warnings.warn('Functionality not tested for all portfolios. Make sure variables contain correct data.')
    return outdf
