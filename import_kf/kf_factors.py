"""
Get factor data from Ken French database.

"""
import numpy as np
import pandas as pd
from pandas.tseries.offsets import MonthEnd, QuarterEnd

import pandas_datareader.data as web

def list_available_files():
    print('''Available factors:

             F-F_Research_Data_Factors (freq='weekly' available)
             F-F_Research_Data_5_Factors_2x3
             F-F_Momentum_Factor
             F-F_ST_Reversal_Factor
             F-F_LT_Reversal_Factor

             Global_3_Factors
             Global_ex_US_3_Factors
             Europe_3_Factors
             Japan_3_Factors
             Asia_Pacific_ex_Japan_3_Factors
             North_America_3_Factors

             Global_5_Factors
             Global_ex_US_5_Factors
             Europe_5_Factors
             Japan_5_Factors
             Asia_Pacific_ex_Japan_5_Factors
             North_America_5_Factors

             Global_Mom_Factor *
             Global_ex_US_Mom_Factor *
             Europe_Mom_Factor *
             Japan_Mom_Factor *
             Asia_Pacific_ex_Japan_MOM_Factor *
             North_America_Mom_Factor *

             * freq='annual' not available for non-US momentum factors - available online.
          ''')

    pass


def kf_factors(ff_factor_file, freq):
    """
    Query Fama-French factors.

    Parameters
    ----------
    ff_factor_file: string
        Name of factor file. See list_available_files() for a list of available factors.
    freq: string
        Data frequency. Must be in ['daily', 'weekly', 'monthly', 'annual'].
        freq='weekly' is only available for ff_factor_file='F-F_Research_Data_Factors'.

    Examples
    --------
    df = kf_factors('F-F_Research_Data_Factors', 'monthly')
    """

    assert freq in ['daily', 'weekly', 'monthly', 'annual'], "freq must be 'daily', 'weekly', 'monthly' or 'annual'"
    if freq == 'weekly':
        assert ff_factor_file == 'F-F_Research_Data_Factors', \
            "freq='weekly' is only available for ff_factor_file='F-F_Research_Data_Factors'"

    if freq == 'annual':
        assert ff_factor_file not in ['Global_Mom_Factor',
                                      'Global_ex_US_Mom_Factor',
                                      'Europe_Mom_Factor',
                                      'Japan_Mom_Factor',
                                      'Asia_Pacific_ex_Japan_MOM_Factor',
                                      ' North_America_Mom_Factor'], \
                                          "freq='annual' not available for {}".format(ff_factor_file)
    if freq == 'daily':
        ff_factor_file = ff_factor_file + '_daily'
    elif freq == 'weekly':
        ff_factor_file = ff_factor_file + '_weekly'

    df = web.DataReader(ff_factor_file, 'famafrench')

    if freq == 'annual':
        df = df[1].reset_index()
    else:
        df = df[0].reset_index()

    df.rename(columns={'Date': 'date'}, inplace=True)
    try:
        df['date'] = df['date'].dt.to_timestamp(how='end')
    except AttributeError:
        pass

    df.replace(to_replace=[-99.99, -999], value=np.nan, inplace=True)

    return df


