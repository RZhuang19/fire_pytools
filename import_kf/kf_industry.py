"""
Get industry portfolio data from Ken French database.

"""
import numpy as np
import pandas as pd
pd.core.common.is_list_like = pd.api.types.is_list_like
import pandas_datareader as web

VARIABLE_KEY_INDUSTRIES = {'daily': {'ret': 0,
                                     'ret_ew': 1},
                           'monthly': {'ret': 0,
                                       'ret_ew': 1,
                                       'n_firms': 4,
                                       'avg_size': 5},
                           'annual': {'ret': 2,
                                      'ret_ew': 3,
                                      'beme_sum': 6,
                                      'beme_vw': 7}
                           }


def download(n_industries, start_date, end_date, freq):
    """
    Query Fama-French industry portfolios and transform to long format.

    Parameters
    ----------
    n_industries: integer
        Number of industry portfolios. Must be in [5, 10, 12, 17, 30, 38, 48, 49].
    start_date: string
        Sample start date.
    end_date: string
        Sample end date.
    freq: string
        Data frequency. Must be in ['daily', 'monthly', 'annual'].

    Examples
    --------
    df = download(49, '1-1-2000', '12-31-2017', 'monthly')
    """

    assert n_industries in [5, 10, 12, 17, 30, 38, 48, 49], \
        'Number of FF industries must be in [5, 10, 12, 17, 30, 38, 48, 49]'

    assert freq in ['daily', 'monthly', 'annual'], "freq must be 'daily', 'monthly' or 'annual'"

    if freq == 'daily':
        table = '{}_Industry_Portfolios_daily'.format(n_industries)
    else:
        table = '{}_Industry_Portfolios'.format(n_industries)

    df = web.DataReader(table, 'famafrench', start=start_date, end=end_date)

    for key, elem in VARIABLE_KEY_INDUSTRIES[freq].items():
        tempdf = df[elem].reset_index()

        # Remove trailing whitespaces from column names
        tempdf.columns = tempdf.columns.str.strip()

        tempdf = tempdf.melt(id_vars='Date', var_name='portfolio',
                             value_name=key)
        if 'outdf' not in locals():
            outdf = tempdf
        else:
            outdf = outdf.merge(tempdf, on=['Date', 'portfolio'])

    outdf.rename(columns={'Date': 'date'}, inplace=True)
    try:
        outdf['date'] = outdf['date'].dt.to_timestamp(how='end')
    except AttributeError:
        pass

    outdf.replace(to_replace=[-99.99, -999], value=np.nan, inplace=True)

    return outdf
