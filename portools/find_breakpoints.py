#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python
# coding: utf-8

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Data: 2019-02
"""


# %% Packages
import pandas as pd
import numpy as np

# %% Function Definition


def find_breakpoints(data, quantiles, id_variables, exch_cd=None, silent=False):
    """Calculates breakpoints.

    Parameters:
    -----------
    data: DataFrame
    quantiles: dictionary
        Format is {'VARIABLE_TO_BE_SORTED_ON': [quantile]}.
    id_variables: list
        Format is ['DATE','PERMNO','EXCHCD']. 'EXCHCD' is necessary only if exch_cd is defined.
    exch_cd: list, optional
        Exchage code to use as reference for calculation of the breakpoints - usually NYSE [1].
    silent: boolean, default False
        If True, print status reports to console.

    Return:
    -------
    DataFrame with breakpoints per date and quantile bin.

     Examples
    --------
    port_breakpoints = find_breakpoints(data = adata,
                                        quantiles = {'mesum_dec':[0.5], 'beme':[0.3,0.7]},
                                        id_variables = ['fyear', 'permno', 'exchcd'],
                                        exch_cd = [1])
    """

    EXCHCD_dict = {1: 'NYSE', 2: 'American Stock Exchange', 3: 'Nasdaq'}
    var_name = list(quantiles.keys())[0]

    if exch_cd:
        data = data[data[id_variables[2]].isin(exch_cd)]

    grouped = data.groupby(id_variables[0])
    data_bp = grouped[var_name].quantile(quantiles[var_name]).unstack()
    data_bp.reset_index(inplace=True)

    if silent is False:
        if exch_cd:
            if len(exch_cd) == 1:
                print("%s breakpoints were calculated for %s." % (EXCHCD_dict[exch_cd[0]], var_name))
            else:
                print("Breakpoints were calculated for %s. We considered stocks listed on EXCHCD %s" % (var_name, str(exch_cd)))
        else:
            print("Breakpoints were calculated for %s." % var_name)

    return data_bp
