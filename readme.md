# (FI)nance (RE)search Python Tools

This is a collection of useful functions in finance research. 

1. Direct download of CRSP and Compustat from WRDS server;
2. Direct download of Ken French data library;
2. Portfolio sorts tools.

### Prerequisites

Before using the WRDS direct download you need to set up your access [here](https://wrds-www.wharton.upenn.edu/pages/support/programming-wrds/programming-python/python-from-your-computer/). You need a WRDS subscription.

## Examples

### Calculate Portfolio Breakpoints 

In finance research, very often we have to sort stocks into buckets according to a stock characteristic. The function find_breakpoints calculates the breakpoints.

#### 
```
import pandas as pd
import numpy as np

from portools.find_breakpoints import *

adata = pd.DataFrame({'permno': [1001, 2002]*5,
					  'rankyear': [2000]*2 + [2001]*2 + [2002]*2 + [2003]*2 + [2004]*2,
					  'exch_cd': 1,
					  'me': np.linspace(1, 10, 10),
					  'beme': np.linspace(0.10, 2, 10)})

# Calculate me Breakpoints
bp_me = find_breakpoints(data=adata,
						 quantiles={'me': [0.5]},
						 id_variables=['rankyear', 'permno', 'exch_cd'])
```

### Portfolio Sorts  
You can also associate each stock/date to a portfolio bucket using the "sort_portfolios" function.

####
```
from portools.sort_portfolios import *

# Sort Portfolios
port = sort_portfolios(data=adata,
					   quantiles={'me': [0.5], 'beme': [0.5]},
					   id_variables=['rankyear', 'permno', 'exch_cd'])
```



## Authors

* **Lira Mota ** - [liramota](https://bitbucket.org/liramota/)
* **Simon Rottke ** - [simon0222](https://bitbucket.org/simon0222/)


## Acknowledgments

The code posted here is the result of continuous collaboration with our co-authos and colleagues. 

All errors are our own -  but we am very grateful to receive your help to fix them =) Please email us if you find errors or more efficient solutions. 

Important inspiration and/or collaboration:

* Kent Daniel
* Tano Santos
* Tuomas Tomunen 


