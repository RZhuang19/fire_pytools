#!/usr/bin/env /share/share1/share_dff/anaconda3/bin/python

"""
Author: Lira Mota, lmota20@gsb.columbia.edu
Date: 2019-02
Code:
    Creates stock_monthly pandas data frame.
    Import CRSP MSE and MSF and risk free returns.
    Calculates returns adjusted for delisting, market equity and excess returns.
"""

# %% Packages
import wrds
import pandas as pd
import numpy as np
import datetime

from import_wrds.crsp_sf import *
from utils.post_event_nan import *
from utils.monthly_date import *

# %% Auxiliary Functions


def calculate_melag_weight(mdata):
    """
     Parameters:
    ------------
    mdata: data frame
        crsp monthly data with cols permno, date as index and lag_me column

    Notes:
    ------
    Return a filled MElag to be used as portfolios weights. The difference is that we need to pad missings.

    If ME is missing, we do not exclude stock, but rather keep it in with last non-missing MElag.
    The stock will be excluded if:
    (i) Delisted;
    (ii) Have a missing ME in the moment of portfolio construction.

    This is different than Ken's method

    EXAMPLE:
    --------
    there seem to be 12 stocks with missing PRC and thus missing ME in July 1926.
    Thus, our number of firms drops from 428 to 416.
    Fama and French report 427 in both July and August, so they also do not seem to exclude these
    rather they probably use the previous MElag for weight and must assume some return in the following month.

    The whole paragraph from the note on Ken French's website:
    ----------------------------------------------------------
    "In May 2015, we revised the method for computing daily portfolio returns
    to match more closely the method for computing monthly portfolio returns.
    Daily files produced before May 2015 drop stocks from a portfolio
    (i) the next time the portfolio is reconstituted, at the end of June, regardless of the CRSP delist date or
    (ii) during any period in which they are missing prices for more than 10 consecutive trading days.
    Daily files produced after May 2015 drop stocks from a portfolio
    (i) immediately after their CRSP delist date or
    (ii) during any period in which they are missing prices for more than 200 consecutive trading days. "
    """

    required_cols = ['lag_me', 'lag_dlret']

    set(required_cols).issubset(mdata.columns), "Required columns: {}.".format(', '.join(required_cols))

    df = mdata[required_cols].copy()
    df['melag'] = df.groupby('permno').lag_me.fillna(method='pad')
    df.reset_index(inplace=True)

    # Fill na after delisting
    df = post_event_nan(df=df, event=df.lag_dlret.notnull(), vars=['melag'], id_vars=['permno', 'date'])

    df.set_index(['permno', 'date'], inplace=True)

    return df[['melag']]

# %% Main Function


def main():
    
    print('Stock monthly calculation started.')

    # %% Set Up
    db = wrds.Connection(wrds_username='lmota')  # make sure to configure wrds connector before hand.

    start_time = time.time()

    # %% Download CRSP data
    varlist = ['dlret', 'dlretx', 'exchcd', 'naics', 'permco', 'prc', 'ret', 'shrcd', 'shrout', 'siccd', 'ticker']

    start_date = '1926-01-01'  # '2017-01-01' #
    end_date = datetime.date.today().strftime("%Y-%m-%d")
    freq = 'monthly'  # 'daily'
    permno_list = None  # [10001, 14593, 10107] #
    shrcd_list = None  # [10, 11] #
    exchcd_list = None  # [1, 2, 3] #
    crspm = crsp_sf(varlist,
                    start_date,
                    end_date,
                    freq=freq,
                    permno_list=permno_list,
                    shrcd_list=shrcd_list,
                    exchcd_list=exchcd_list,
                    db=db)

    query = "SELECT caldt as date, t30ret as rf FROM crspq.mcti"
    rf = db.raw_sql(query, date_cols=['date'])
    del query

    # %% Create variables

    # Rankyear
    # Rankyear is the year where we ranked the stock, e.g., for the return of a stock in January 2001,
    # rankyear is 2000, because we ranked it in June 2000
    crspm['rankyear'] = crspm.date.dt.year
    crspm.loc[crspm.date.dt.month <= 6, 'rankyear'] = crspm.loc[crspm.date.dt.month <= 6, 'rankyear'] - 1

    # Returns adjusted for delisting
    crspm['retadj'] = ((1 + crspm['ret'].fillna(0)) * (1 + crspm['dlret'].fillna(0)) - 1)
    crspm.loc[crspm[['ret', 'dlret']].isnull().all(axis=1), 'retadj'] = np.nan

    # Create Market Equity (ME)
    # SHROUT is the number of publicly held shares, recorded in thousands. ME will be reported in 1,000,000 ($10^6$).
    # If the stock is delisted, we set ME to NaN.
    crspm['me'] = abs(crspm['prc']) * (crspm['shrout'] / 1000)

    # Adjust for delisting
    crspm.loc[crspm.dlret.notnull(), 'me'] = np.nan

    # Create melag (portfolio weights)
    crspm.sort_values(['permno', 'date'])
    crspm.set_index(['permno', 'date'], inplace = True)
    crspm[['lag_me', 'lag_dlret']] = crspm.groupby('permno')[['me', 'dlret']].shift(1)
    crspm['melag_weights'] = calculate_melag_weight(crspm)
    crspm.drop(columns=['lag_me', 'lag_dlret'], inplace=True)

    crspm.reset_index(inplace=True)

    # %% Add Risk Free
    rf['mdate'] = rf.date.apply(monthly_date)
    crspm['mdate'] = crspm.date.apply(monthly_date)

    rf.drop(columns='date', inplace=True)

    stock_monthly = pd.merge(crspm, rf, on='mdate', how='left')
    stock_monthly.rf.isnull().sum()

    print("Time to create CRSP monthly: %s minutes" % str(np.round((time.time() - start_time)/60, 2)))

    return stock_monthly

# %% Main


if __name__ == '__main__':
    main()





