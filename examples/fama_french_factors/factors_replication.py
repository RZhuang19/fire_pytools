"""
Author: Lira Mota
Date: 2019-02
Code: Replicates Fama and French 5 Factor (work in progress)
"""

#%% Packages

import pandas as pd
from pylab import *
from matplotlib.pyplot import figure

import examples.fama_french_factors.stock_annual as stock_annual
import examples.fama_french_factors.stock_monthly as stock_monthly

from portools.sort_portfolios import sort_portfolios

idx = pd.IndexSlice

# %% Set Up

char_breakpoints = {'ME': [0.5],
                    'BEME': [0.3, 0.7],
                    'OP': [0.3, 0.7],
                    'INV': [0.3, 0.7]}

weightvar = 'melag_weight'

retvar = 'retadj'

rankvar = 'rankyear'

dict_factors = {'BEME': 'HML',
                'ME': 'SMB',
                'OP': 'RMW',
                'INV': 'CMA'}

# %% Download Data
# FF five factor

# Annual Data
adata = stock_annual.main()

# Monthly Data
mdata = stock_monthly.main()


# %% Find Breakpoints

# %% Portfolio Sorts

# %% Portfolio Returns

# %% Checks








